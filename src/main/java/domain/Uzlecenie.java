package domain;

public class Uzlecenie extends UoPrace {
	
	private KosztUzyskPrzych kosztUzyskPrzych;
	private boolean rentowa;
	private boolean emerytalna;
	private boolean chorobowa;
	private boolean zdrowotna;
	
	public KosztUzyskPrzych getKosztUzyskPrzych() {
		return kosztUzyskPrzych;
	}
	public void setKosztUzyskPrzych(KosztUzyskPrzych kosztUzyskPrzych) {
		this.kosztUzyskPrzych = kosztUzyskPrzych;
	}
	public boolean isRentowa() {
		return rentowa;
	}
	public void setRentowa(boolean rentowa) {
		this.rentowa = rentowa;
	}
	public boolean isEmerytalna() {
		return emerytalna;
	}
	public void setEmerytalna(boolean emerytalna) {
		this.emerytalna = emerytalna;
	}
	public boolean isChorobowa() {
		return chorobowa;
	}
	public void setChorobowa(boolean chorobowa) {
		this.chorobowa = chorobowa;
	}
	public boolean isZdrowotna() {
		return zdrowotna;
	}
	public void setZdrowotna(boolean zdrowotna) {
		this.zdrowotna = zdrowotna;
	}
	
	
}
