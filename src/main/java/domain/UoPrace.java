package domain;


import java.util.*;

public class UoPrace {
	
	private int rok;
	private int kwota;
	private String rodzajKwoty;
	private List <String> miesiace;
	
	public UoPrace() {
		miesiace = new ArrayList<String>();
		miesiace.add("Styczeń");
		miesiace.add("Luty");
		miesiace.add("Marzec");
		miesiace.add("Kwiecień");
		miesiace.add("Maj");
		miesiace.add("Czerwiec");
		miesiace.add("Lipiec");
		miesiace.add("Sierpień");
		miesiace.add("Wrzesień");
		miesiace.add("Październik");
		miesiace.add("Listopad");
		miesiace.add("Grudzień");
	}
	
	public int getRok() {
		return rok;
	}
	public void setRok(int rok) {
		this.rok = rok;
	}
	public int getKwota() {
		return kwota;
	}
	public void setKwota(int kwota) {
		this.kwota = kwota;
	}

	public String getRodzajKwoty() {
		return rodzajKwoty;
	}

	public void setRodzajKwoty(String rodzajKwoty) {
		this.rodzajKwoty = rodzajKwoty;
	}

	public List<String> getMiesiace() {
		return miesiace;
	}

	public void setMiesiace(List<String> miesiace) {
		this.miesiace = miesiace;
	}
		

}
