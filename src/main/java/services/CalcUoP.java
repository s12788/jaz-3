package services;

import java.text.DecimalFormat;

import domain.*;

public class CalcUoP {
	
	private double emerytalne;
	private double rentowe;
	private double chorobowe;
	private double podstawa;
	private double zaliczkaPITdoZaplaty;
	private double zdrowotne;
	private double netto;
	
	public CalcUoP (UoPrace uoPrace){
		emerytalne(uoPrace);
		rentowe(uoPrace);
		chorobowe(uoPrace);
		podstawa(uoPrace);
		zdrowotne(uoPrace);
		zaliczkaPIT(uoPrace);
		netto(uoPrace);
	}
	
	
	private void emerytalne (UoPrace uoPrace){
		
		emerytalne = round(uoPrace.getKwota()*0.0976);	
	}
	
	private void rentowe (UoPrace uoPrace){
		
		rentowe = round(uoPrace.getKwota()*0.015);	
	}
	
	private void chorobowe (UoPrace uoPrace){
		
		chorobowe = round(uoPrace.getKwota()*0.0245);		
	}
	
	private void zdrowotne (UoPrace uoPrace){
		
		zdrowotne = round((podstawa+111.25)*0.09);	
	}
	
	private void podstawa (UoPrace uoPrace){
		
		double skladki = chorobowe+emerytalne+
				rentowe;
		podstawa = uoPrace.getKwota()-skladki-111.25;
	}
	
	private void zaliczkaPIT (UoPrace uoPrace){
		
		double podatek = podstawa*0.18;
		double zaliczkaPIT = podatek-46.33;
		double zdrowotneDoOdliczenia = (podstawa+111.25)*0.0775;
		zaliczkaPITdoZaplaty = Math.round(zaliczkaPIT-zdrowotneDoOdliczenia);
	}
	
	private void netto (UoPrace uoPrace) {
		
		netto = podstawa-zdrowotne-zaliczkaPITdoZaplaty+111.25;
}
	
	private double round (double value){
		
		double rounded = (double) Math.round(value * 100.00) / 100.00;
		return rounded;
	}
	
	public String decimalFormat (double value){
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format(value);
	}


	public String getEmerytalne() {
		return decimalFormat(emerytalne);
	}


	public String getRentowe() {
		return decimalFormat(rentowe);
	}


	public String getChorobowe() {
		return decimalFormat(chorobowe);
	}


	public String getPodstawa() {
		return decimalFormat(podstawa);
	}


	public String getZaliczkaPITdoZaplaty() {
		return decimalFormat(zaliczkaPITdoZaplaty);
	}


	public String getZdrowotne() {
		return decimalFormat(zdrowotne);
	}


	public String getNetto() {
		return decimalFormat(netto);
	}
	
	
	
	
}
