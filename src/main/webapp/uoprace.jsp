<%@page import="services.CalcUoP"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Kalkulator płac</title>
</head>
<body>

      <jsp:useBean id="uOprace" class="domain.UoPrace" scope="session"/>
<%--       <jsp:useBean id="calcuop" class="services.CalcUoP" scope="session"/> --%>
      
      <jsp:setProperty property="*" name="uOprace"/>

	Kwota: ${uOprace.kwota}
	Rok: ${uOprace.rok}
	Rodzaj: ${uOprace.rodzajKwoty}
	
	<%CalcUoP calcUoP = new CalcUoP(uOprace); %>

	<h3>Umowa o pracę</h3>
		
<table class='t'>
	<tr>
		<th rowspan="2">Miesiąc</th>
		<th rowspan="2">Brutto</th>
		<th colspan="4">Ubezpieczenie</th>
		<th rowspan="2">Podstawa opodatkowania</th>
		<th rowspan="2">Zaliczka na PIT</th>
		<th rowspan="2">Netto</th>
	</tr>
	<tr>
			<th>emerytalne</th>
			<th>rentowe</th>
			<th>chorobowe</th>
			<th>zdrowotne</th>
	</tr>
	<%
	for (String c: uOprace.getMiesiace()){
		%>
		<tr>
			<th><%=c%></th><th><%=uOprace.getKwota()%></th>
			<th><%=calcUoP.getEmerytalne()%></th><th><%=calcUoP.getRentowe()%></th>
			<th><%=calcUoP.getChorobowe()%></th><th><%=calcUoP.getZdrowotne()%></th>
			<th><%=calcUoP.getPodstawa()%></th><th><%=calcUoP.getZaliczkaPITdoZaplaty()%></th>
			<th><%=calcUoP.getNetto()%></th>
		</tr>
	<% } %>

</table>
	

</body>
</html>